OAMPRS

Is a service to connect OAMS and APRS users.

OAMPRS shows up as "APRS" in the OAMS chat list. Users can send a message to APRS, the first word of the message being the callsign of the APRS station they wish to send a message to.

APRS users can send a message to OAMS, the first word of the message being the callsign of the station they wish to send a message to. 



Programming structure
Following good practice, generic libraries for connecting to each service live in their respective header (.hpp) files. The main.cpp only holds the logic to connec the two services to each other. 


Uses [websocketpp](https://www.zaphoyd.com/projects/websocketpp/)


License
OAMPRS is licensed under the GPLv3 and freely available under the terms of the license. The author makes not guarantee or warrantees as to the suitablity of the code for any particular use and holds no liablity for damage or losses arrising from the use of any code in OAMPRS. 



